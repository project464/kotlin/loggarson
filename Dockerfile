ARG VERSION=8u151

FROM openjdk:${VERSION}-jdk as BUILD
COPY . /src
WORKDIR /src
RUN ./gradlew
RUN ./gradlew --no-daemon fatJar

FROM openjdk:${VERSION}-jre

COPY --from=BUILD /src/build/libs/adhetelbot-1.0-SNAPSHOT.jar /bin/runner/run.jar
WORKDIR /bin/runner

CMD ["java","-jar","run.jar"]


#FROM java:8
#WORKDIR /application
#ADD ./build/libs/adhetelbot-1.0-SNAPSHOT.jar /application/app.jar
#CMD java -jar /application/app.jar

