package configs

import main.configs.Constants.chatsConfigurationFilePath
import main.configs.Logger
import main.chatConfigCache
import main.chatConfigCache.ChatConfig
import java.io.File
import java.io.IOException



// Работаем с хостами
// Добавили хост
fun addChatNewHost(chatId: Long, host: String): ChatConfig {
    val chatConfig = readChatConfigFile(chatId)
// Добавили
    chatConfig.serverList.add(host)
//Записали в файл
    writeToChatConfigFile(chatConfig)
//Вернули прочитав еще раз.
    Logger.d(readChatConfigFile(chatId))
    //обновляем кэш
    chatConfigCache.updateUserCache()
    return readChatConfigFile(chatId)

}


// Удалили хост
fun removeChatConfigHost(chatId: Long, hostNumber: Int) {
    val chatConfig = readChatConfigFile(chatId)
    Logger.d("Удаление хоста: " + chatConfig.serverList[hostNumber])
    chatConfig.serverList.removeAt(hostNumber)
    writeToChatConfigFile(chatConfig)
    //обновляем кэш
    chatConfigCache.updateUserCache()
}

//Поверяем что есть папка для хранения файла
fun checkChatConfigFile(chatId: Long) {
    File(chatsConfigurationFilePath).mkdir()
    val configFile = File(chatsConfigurationFilePath, chatId.toString())
    if(!configFile.exists()) {
        configFile.createNewFile()
    }
}
//Читаем фийл
fun readChatConfigFile(chatId: Long): ChatConfig {
    Logger.d("Зашли в readChatConfigFile")
    checkChatConfigFile(chatId)
    val cConfig = ChatConfig(chatId)
    Logger.d(cConfig)
    try {
        File(chatsConfigurationFilePath, chatId.toString()).forEachLine {
            Logger.d(it)
            cConfig.serverList.add(it)
        }
    } catch (e: IOException) {
        Logger.e(e)
    }
    return cConfig
}
//Пишем изменения в конфигурационный файл
fun writeToChatConfigFile(cconfig: ChatConfig) {
    val chatConfigFile = File(chatsConfigurationFilePath, cconfig.chatId.toString())
    chatConfigFile.delete()
    cconfig.serverList.forEach {
        chatConfigFile.appendText("$it\n")
    }
    //обновляем кэш
    chatConfigCache.updateUserCache()
}


