package main

import configs.readChatConfigFile
import main.configs.Constants.chatsConfigurationFilePath
import java.io.File

object chatConfigCache {
    data class ChatConfig(
        var chatId: Long,
        var serverList: MutableList<String> = mutableListOf()
    )

    var usersList = mutableListOf<ChatConfig>()

    fun loadUserCache() {
        File(chatsConfigurationFilePath).listFiles().forEach {
            println(it)
            usersList.add(readChatConfigFile(it.name.toLong()))
        }
        println(usersList)
    }
    fun updateUserCache(){
        usersList.clear()
        loadUserCache()
    }
}