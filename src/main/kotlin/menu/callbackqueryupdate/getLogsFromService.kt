package main.commands.bot

import main.menu.Commands
import main.configs.Logger
import main.workers.ssh.returnLogs
import org.telegram.telegrambots.meta.api.methods.send.SendDocument
import org.telegram.telegrambots.meta.api.objects.Update

internal fun Commands.getLogsFromService(update: Update) {
    Logger.d("Entering getLogsFromService")
    Logger.d("Полученные ответ: " + update.callbackQuery.toString())
    val data = update.callbackQuery.data.split(":")
    Logger.d(data)
    Logger.d(update.callbackQuery.message.chatId.toString())


    var serviceLogs = returnLogs(
        data[2],
        update.callbackQuery.message.chatId,
        data[1].toInt()
    )
    Logger.d(serviceLogs.toString())
    execute(
        SendDocument().setChatId(
            update.callbackQuery.message.chatId
        ).setDocument(serviceLogs.logFile)
    )
    serviceLogs.logFile.delete()
}