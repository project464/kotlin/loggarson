package main.commands

import main.chatConfigCache
import main.commands.bot.chooseHostToGetServicesList
import main.commands.bot.chooseServiceToGetLogs
import main.menu.Commands
import main.configs.Logger
import main.workers.ssh.returnDockerPs
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update

internal fun Commands.botCommands(update: Update?) {

    Logger.d("зашли в ботКомандс")
    var msg = update!!.message.text.split(" ").map { it.trim() }

    if (msg.size == 1) {
        execute(
            SendMessage()
                .setChatId(update.message.chatId)
                .setText("Укажите команды, которые необходимо выполнить.")
        )
    } else {
        when {
            msg.contains("docker") -> {
                when {
                    msg.contains("ps") -> {
                        Logger.d("Зашли в  ps")
                        execute(
                            SendMessage().setChatId(update.message.chatId)
                                .setText(returnDockerPs(update.message.chatId).joinToString("\n"))
                        )
                    }
                    msg.contains("logs") -> {
                        Logger.d("Зашли в логи")
//Если в записи только 1 хост, то мы сразу выдаем возможность выбрать название сервиса
//логи которого можно получить
                        when {
                            chatConfigCache.usersList.filter{ it.chatId == update.message.chatId}[0].serverList.size == 1 -> chooseServiceToGetLogs(update)
// Если же в чате не один хост, то сначала предлагаем выбрать хост на котором нужно посмотреть логи
// а затем даем возможность выбрать сервис логи которого нужно посмотреть.
                            else -> {
                                Logger.d("пришли туда")
                                chooseHostToGetServicesList(update)
                            }
                        }
                    }
                }
            }
        }
    }
}
