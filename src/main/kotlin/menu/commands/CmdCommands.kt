package main.commands

import main.menu.Commands
import main.configs.Constants
import main.configs.Logger
import main.workers.ssh.executeCommandOnRemoteHost
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update


internal fun Commands.cmdCommands(update: Update?) {
    val msg = update!!.message.text.split(" ").map { it.trim() }
    if (!Constants.adminId.contains(update.message.from.id.toString())) {
        Logger.w("${update.message.from.id} пытался выполнить команду незаконно.")
        execute(
            SendMessage()
                .setChatId(update.message.chatId)
                .setText("${update.message.from.firstName}, хорошая попытка, бро.")
        )
    } else {
        if (msg.size == 1) {
            execute(
                SendMessage()
                    .setChatId(update.message.chatId)
                    .setText(
                        "Укажите сервер и команды, которые необходимо выполнить.\n" +
                                "пример /cmd 2 test your mind"
                    )
            )
        } else {
            var hostSetAsDigit = true
            try {
                val num = msg[1]
            } catch (e: NumberFormatException) {
                hostSetAsDigit = false
            }
            when (hostSetAsDigit) {
                true -> {
                    execute(
                        SendMessage().setChatId(update.message.chatId)
                            .setText(
                                executeCommandOnRemoteHost(
                                    update.message.chatId,
                                    msg.subList(2, msg.size).joinToString(" "),
                                    msg[1].toInt()
                                ).joinToString("\n")
                            )
                    )
                }
                false -> {
                    execute(
                        SendMessage().setChatId(update.message.chatId)
                            .setText(
                                executeCommandOnRemoteHost(
                                    update.message.chatId,
                                    msg.subList(1, msg.size).joinToString(" ")
                                ).joinToString("\n")
                            )
                    )
                }
            }
        }
    }
}
