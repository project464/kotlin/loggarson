package main.commands

import main.menu.Commands
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow

internal fun Commands.helpCommand(update: Update?) {
    val keyboard = ReplyKeyboardMarkup()
    keyboard.resizeKeyboard = true
    keyboard.keyboard = listOf(
        KeyboardRow().apply {
            add(KeyboardButton("/bot docker ps"))
        }, KeyboardRow().apply {
            add(KeyboardButton("/bot docker logs"))
        }
    )
    execute(
        SendMessage()
            .setChatId(update!!.message.chatId)
            .setReplyMarkup(keyboard)
            .setText(
                "Доступные команды: " +
                        "\n выполнить - `cmd SERVER COMMANDTOEXECUTE`" +
                        "\n настроить - `set (ADD|REMOVE) USER@HOSTADRESS`"
            )
    )
}