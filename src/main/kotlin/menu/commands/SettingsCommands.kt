package main.commands

import configs.*
import main.configs.Constants
import main.configs.Logger
import main.chatConfigCache
import main.menu.Commands
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update

internal fun Commands.setCommands(update: Update?) {
    val msg = update!!.message.text.split(" ").map { it.trim() }

    if (!Constants.adminId.contains(update.message.from.id.toString())) {
        execute(
            SendMessage()
                .setChatId(update.message.chatId)
                .setText("${update.message.from.firstName}, у вас нет доступа к выполненению команд.")
        )
    } else {
        if (msg.size == 1) {
            execute(
                SendMessage()
                    .setChatId(update.message.chatId)
                    .setText("Доступные команды: \nadd\nremove")
            )
        } else {
            when (msg[1]) {
                "add" -> {
                    if (msg.size == 2) {
                        execute(
                            SendMessage()
                                .setChatId(update.message.chatId)
                                .setText("Вы не указали хост")
                        )

                    } else {
                        addChatNewHost(update.message.chatId, msg[2])
                        execute(
                            SendMessage()
                                .setChatId(update.message.chatId)
                                .setText(
                                    "Хост ${msg[2]} добавлен.\n" +
                                            "В этом чате достуны следующие хосты:\n" +
                                            "${chatConfigCache.usersList.filter { it.chatId == update.message.chatId }[0].serverList}"
                                )
                        )
                    }
                }
                "remove" -> {
                    Logger.d(msg[2])
                    Logger.d("Заходим в  удаление хоста")
                    if (msg.size < 3) {
                        Logger.d("Говорим что не указан номер хоста")
                        execute(
                            SendMessage()
                                .setChatId(update.message.chatId)
                                .setText("Вы не указали номер хоста")
                        )
                    } else {
                        Logger.d("Пытаемся удалить хост: " + msg[2])
                        if (msg[2].toInt() >= chatConfigCache.usersList.filter { it.chatId == update.message.chatId }[0].serverList.size) {
                            execute(
                                SendMessage()
                                    .setChatId(update.message.chatId)
                                    .setText("Вы пытаетесь удалить несуществующий хост")
                            )

                        } else {
                            removeChatConfigHost(
                                update.message.chatId,
                                msg[2].toInt()
                            )
                            Logger.d("Пишем оповещение в чатик с новым списком хостов.")
                            execute(
                                SendMessage()
                                    .setChatId(update.message.chatId)
                                    .setText(
                                        "Хост ${msg[2]} удален.\n" +
                                                "Список хостов: /info"
                                    )
                            )
                        }
                    }
                }
                else -> {
                    execute(
                        SendMessage()
                            .setChatId(update.message.chatId)
                            .setText("Доступные команды: \nadd\nremove")
                    )

                }
            }

        }
    }
}