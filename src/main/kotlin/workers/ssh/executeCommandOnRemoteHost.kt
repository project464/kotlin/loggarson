package main.workers.ssh

import main.chatConfigCache
import main.configs.Logger
import java.io.BufferedReader
import java.io.InputStreamReader

fun executeCommandOnRemoteHost(chatId: Long, command: String, serverNumber: Int = 0 ): MutableList<String?>
{
    Logger.d("Entering executeCommandOnRemoteHost")
    val containersStatus= mutableListOf<String?>()
    val rt = Runtime.getRuntime()
    val cConfig = chatConfigCache.usersList.filter { it.chatId == chatId }[0]
    val ecommand = "ssh -o ForwardX11=no ${cConfig.serverList[serverNumber]} <<- EOF\n" +
            "$command\n" +
            "exit\n" +
            "EOF"
    Logger.d(ecommand)
    val proc = rt.exec(ecommand)
    val stdInput = BufferedReader(InputStreamReader(proc.inputStream))
    val stdError = BufferedReader(InputStreamReader(proc.errorStream))
// Read the output from the command
    var s: String?
    while (stdInput.readLine().also { s = it } != null) {
        containersStatus.add(s)
    }
// Read any errors from the attempted command
    while (stdError.readLine().also { s = it } != null) {
        containersStatus.add(s)
    }
    return containersStatus

}
