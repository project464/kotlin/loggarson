package main.workers.ssh

import main.chatConfigCache
import java.io.BufferedReader
import java.io.InputStreamReader

fun returnDockerPs(chatId: Long, serverNumber: Int = 0 ): MutableList<String?> {
    val containersStatus= mutableListOf<String?>()
    val rt = Runtime.getRuntime()
    val cConfig = chatConfigCache.usersList.filter { it.chatId == chatId }[0]
    val command = "ssh -o ForwardX11=no ${cConfig.serverList[serverNumber]} docker ps --format \"{{.Names}}\\\t{{.Ports}}\\\t{{.Status}}\""
    val proc = rt.exec(command)
    val stdInput = BufferedReader(InputStreamReader(proc.inputStream))
    val stdError = BufferedReader(InputStreamReader(proc.errorStream))
// Read the output from the command
    var s: String?
    while (stdInput.readLine().also { s = it } != null) {
        containersStatus.add(s)
    }
// Read any errors from the attempted command
    while (stdError.readLine().also { s = it } != null) {
        containersStatus.add(s)
    }
    return containersStatus
}
